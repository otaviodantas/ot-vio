#include <SD.h>
#include <SPI.h>
#include <Wire.h>
#include <DS3231.h>
#include <MFRC522.h>
#include <DueTimer.h>
#include <Ethernet.h>
#include <PubSubClient.h>
#include <LiquidCrystal_I2C.h>

#define CLIENT_ID   "ArduinoDue"
#define TOPICO_SUBSCRIBE  "Recebe"
#define TOPICO_PUBLISH   "Publica"
#define SS_PIN 52
#define RST_PIN 9

MFRC522 mfrc522(SS_PIN, RST_PIN);

IPAddress ip2 (192, 168, 7, 91);

int lichtstatus;
byte mac[] = {0xB0, 0xCD, 0xAE, 0x0F, 0xDE, 0x10};
String v = "trsada";
String ip = "";

const char* BROKER_MQTT = "200.129.242.4";
int BROKER_PORT = 20049;

EthernetClient ethClient;
PubSubClient mqttClient;

void setup() 
{
  Serial.begin(9600);
  
  SPI.begin(4);
  SPI.begin(SS_PIN);
  
  //pinMode(SS_PIN, INPUT);
  while (!Serial) {};
  
  Serial.println(F("MQTT Arduino Demo"));
  Serial.println();
  
  if (Ethernet.begin(mac) == 0)  // Start in DHCP Mode
  {
    Serial.println("Failed to configure Ethernet using DHCP, using Static Mode");
    Ethernet.begin(mac, ip2);
  }
  
  Serial.println(F("Ethernet configurada via DHCP"));
  Serial.print("IP address: ");
  Serial.println(Ethernet.localIP());
  Serial.println();

  ip = String (Ethernet.localIP()[0]);
  ip = ip + ".";
  ip = ip + String (Ethernet.localIP()[1]);
  ip = ip + ".";
  ip = ip + String (Ethernet.localIP()[2]);
  ip = ip + ".";
  ip = ip + String (Ethernet.localIP()[3]);

  initMQTT();
  reconnectMQTT();
  mfrc522.PCD_Init();
  Serial.println("Aproxime o cartao!");
}

void initMQTT()
{
  mqttClient.setClient(ethClient);
  Serial.println(F("Dados MQTT cetados"));
  mqttClient.setServer(BROKER_MQTT, BROKER_PORT);
  mqttClient.setCallback(mqtt_callback);
}

void reconnectMQTT()
{
  while (!mqttClient.connected())
  {
    Serial.print("* Tentando se conectar ao Broker MQTT: ");
    Serial.println(BROKER_MQTT);

    if (mqttClient.connect(CLIENT_ID))
    {
      Serial.println("Conectado com sucesso ao broker MQTT!");
      mqttClient.subscribe(TOPICO_SUBSCRIBE);
    }
    else
    {
      Serial.println("Falha ao reconectar no broker.");
      Serial.println("Havera nova tentatica de conexao em 2s");
      delay(1000);
    }
  }
}

void loop() 
{
  sendData();
  readCard();
  mqttClient.loop();

  if (!mqttClient.connected())
    reconnectMQTT();
}

void readCard()
{
  if (!mfrc522.PICC_IsNewCardPresent())
  {
    return;
  }

  if (!mfrc522.PICC_ReadCardSerial())
  {
    return;
  }

  Serial.print("UID tag :");
  String content = "";
  byte letter;
  for (byte i = 0; i < mfrc522.uid.size; i++)
  {
    Serial.print(mfrc522.uid.uidByte[i] < 0x10 ? " 0" : " ");
    Serial.print(mfrc522.uid.uidByte[i], HEX);
    content.concat(String(mfrc522.uid.uidByte[i] < 0x10 ? " 0" : " "));
    content.concat(String(mfrc522.uid.uidByte[i], OCT));
  }
  Serial.println();
  Serial.print("Tag : ");
}

void sendData() {

  if (mqttClient.connect (CLIENT_ID))
    mqttClient.publish(TOPICO_PUBLISH, v.c_str());

}

void mqtt_callback(char* topic, byte * payload, unsigned int length) {

  String msg = "";
  Serial.print("Messagem recebida [");
  Serial.print(TOPICO_SUBSCRIBE);
  Serial.print("] "); //MQTT_BROKER

  for (int i = 0; i < length; i++) {
    msg += (char)payload [i];
  }
  Serial.println();
  Serial.println(msg);

  /*
    if (strncmp((const char*)payload, "ON", 2) == 0) {
     digitalWrite(ledPin, HIGH);    //
     digitalWrite(relayPin, HIGH);
    }
    if (strncmp((const char*)payload, "OFF", 3) == 0) {
     digitalWrite(relayPin, LOW);     //
     digitalWrite(ledPin, LOW);
    }
    if (strncmp((const char*)payload, "READ", 4) == 0) {
     mqttClient.publish("home/br/nb/relay", (relaystate == LOW) ? "OPEN" : "CLOSED");
    }*/
}
