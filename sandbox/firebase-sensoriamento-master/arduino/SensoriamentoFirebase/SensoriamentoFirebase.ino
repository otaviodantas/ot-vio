#include <WiFi.h>
#include <IOXhop_FirebaseESP32.h>
#include <DHT.h>
#include <Ticker.h>

#define DHT_PIN 2
#define DHTTYPE DHT11

// Coanfigurações de Firebase e Wifi.
#define FIREBASE_HOST "https://organica-88489.firebaseio.com/"
#define FIREBASE_AUTH "8f0vjHFySSGNpULGgl1d3V402LcCUXpQquD0XM4P"

//#define WIFI_SSID "Net_5G59E1AF"
//#define WIFI_PASSWORD "4F59E1AF"

#define WIFI_SSID "trabbbb"
#define WIFI_PASSWORD "trabson99"

//#define WIFI_SSID "Net-Virtua-6929"
//#define WIFI_PASSWORD "20369290"
                           
// Publique a cada 5 min
#define PUBLISH_INTERVAL 1000*60*5

DHT dht(DHT_PIN, DHTTYPE);
Ticker ticker;
bool publishNewState = true;

String fireStatus = "";

void setup() {

  Serial.begin(9600);
  delay(1000);   
  dht.begin();               
  WiFi.begin(WIFI_SSID, WIFI_PASSWORD);                                      //try to connect with wifi
  Serial.print("Conectando: ");
  Serial.println(WIFI_SSID);

  while (WiFi.status() != WL_CONNECTED) {

    Serial.print(".");
    delay(500);
  }

  //temporizador
  ticker.attach_ms(PUBLISH_INTERVAL, publish);

  Serial.println();
  Serial.print("Connected to ");
  Serial.println(WIFI_SSID);
  Serial.print("IP Address is : ");
  Serial.println(WiFi.localIP());                                                      //print local IP address
  Firebase.begin(FIREBASE_HOST, FIREBASE_AUTH);                                       // connect to firebase
  Firebase.setString("LED_STATUS", "OFF");                                          //send initial string of led status
}

void publish(){
  publishNewState = true;
}

void loop() {

  //fireStatus = Firebase.getString("LED_STATUS");                     // get led status input from firebase
  
  if(publishNewState)
  {
    Serial.println("Publish new State");
    
    // Obtem os dados do sensor DHT 
    float humidity = dht.readHumidity();
    float temperature = dht.readTemperature();
  
    if(!isnan(humidity) && !isnan(temperature))
    {
      // Manda para o firebase
      Firebase.pushFloat("temperature", temperature);
      Firebase.pushFloat("humidity", humidity);    
      publishNewState = false;
    }
    else
      Serial.println("Error Publishing"); 
  }
}
