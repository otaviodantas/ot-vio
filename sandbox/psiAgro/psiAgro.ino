#include <WiFi.h>
#include <Wire.h>
#include <OneWire.h>
#include <PubSubClient.h>
#include <DallasTemperature.h>

//FLASH_MODE = dio
//FLASH_SIZE = 2MB

#define ONE_WIRE_BUS 13 //PINO DE LEITURA SENSOR

//char daysOfTheWeek[7][12] = {"Domingo", "Segunda", "Terça", "Quarta", "Quinta", "Sexta", "Sabádo"};

#define TOPICO_SUBSCRIBE "Esp32_reciver"
#define TOPICO_PUBLISH   "Esp32_publsh"
//#define TOPICO_PUBLISH2 "DHT/Umi"
#define ID_MQTT  "esp32"

const char* ssid = "CAP1";
const char* password = "cap2009sala90";

const char* BROKER_MQTT = "200.129.247.247";
int BROKER_PORT = 1880;


WiFiClient espClient;
DeviceAddress sensor1;
OneWire oneWire(ONE_WIRE_BUS);
PubSubClient client(espClient);
DallasTemperature sensors(&oneWire);

int value = 0;
String message;
float tempMin = 999;
float tempMax = 0;

// LED Pin
const int ledPin = 4;

void setup() {
  Serial.begin(115200);

  setup_wifi();
  initMQTT();
  //client.setServer(mqtt_server, 5432);
  client.setCallback(callback);
  pinMode(ledPin, OUTPUT);
  //Serial.println("AQUI");
  iniciSensor();
}
void initMQTT()
{
  client.setServer(BROKER_MQTT, BROKER_PORT);
  client.setCallback(callback);
}

void iniciSensor()
{
  sensors.begin();
  Serial.println("Localizando sensores DS18B20...");
  Serial.print("Foram encontrados ");
  Serial.print(sensors.getDeviceCount(), DEC);
  Serial.println(" sensores.");
  if (!sensors.getAddress(sensor1, 0))
    Serial.println("Sensores nao encontrados !");
  // Mostra o endereco do sensor encontrado no barramento
  Serial.print("Endereco sensor: ");
  mostra_endereco_sensor(sensor1);
  Serial.println();
  Serial.println();
}

void mostra_endereco_sensor(DeviceAddress deviceAddress)
{
  for (uint8_t i = 0; i < 8; i++)
  {
    // Adiciona zeros se necessário
    if (deviceAddress[i] < 16) Serial.print("0");
    Serial.print(deviceAddress[i], HEX);
  }
}

void readSensor()
{
  // Le a informacao do sensor
  sensors.requestTemperatures();
  float tempC = sensors.getTempC(sensor1);
  // Atualiza temperaturas minima e maxima
  if (tempC < tempMin)
  {
    tempMin = tempC;
  }
  if (tempC > tempMax)
  {
    tempMax = tempC;
  }

  /*  Serial.print("Temp C: ");
    Serial.print(tempC);
    Serial.print(" Min : ");
    Serial.print(tempMin);
    Serial.print(" Max : ");
    Serial.println(tempMax);*/

  String tempMQTT = (String)tempC;
  client.publish(TOPICO_PUBLISH, tempMQTT.c_str());
}

void setup_wifi() {
  delay(10);
  Serial.println();
  Serial.print("Connecting to ");
  Serial.println(ssid);

  WiFi.begin(ssid, password);

  while (WiFi.status() != WL_CONNECTED) {
    delay(500);
    Serial.print(".");
  }

  Serial.println("");
  Serial.println("WiFi connected");
  Serial.println("IP address: ");
  Serial.println(WiFi.localIP());
}

void callback(char* topic, byte* payload, unsigned int length) {

  for (int i = 0; i < length; i++) {
    char c = (char)payload[i];
    message += c;
  }
  Serial.println("Tópico => " + String(topic) + " | Valor => " + String(message));

  if (message == "#")
  {
    readSensor();
    //String date;
    //client.publish(TOPICO_PUBLISH, date.c_str());
  }

  message = "";

  //Serial.flush();
}

void reconnect() {
  // Loop until we're reconnected
  while (!client.connected()) {
    Serial.print("Tentando conectar MQTT");
    // Attempt to connect
    if (client.connect("ESP8266Client")) {
      Serial.println("Conectado");
      client.subscribe(TOPICO_SUBSCRIBE);
    } else {
      Serial.print("erro MQTT");
      Serial.print(client.state());
      Serial.println(" tentando novamente em 5s");
      // Wait 5 seconds before retrying
      delay(5000);
    }
  }
}
void loop() {
  if (!client.connected()) {
    reconnect();
  }

  //client.loop();

  //readSensor();

  //long now = millis();
  /*String tempMQTT = "teste";
    client.publish(TOPICO_PUBLISH, tempMQTT.c_str());
    delay(2000);*/
  /*
    if (now - lastMsg > 50000) {

    lastMsg = now;

    // Temperature in Celsius
    temperature = bme.readTemperature();
    // Uncomment the next line to set temperature in Fahrenheit
    // (and comment the previous temperature line)
    //temperature = 1.8 * bme.readTemperature() + 32; // Temperature in Fahrenheit

    // Convert the value to a char array
    char tempString[8];
    dtostrf(temperature, 1, 2, tempString);
    Serial.print("Temperature: ");
    Serial.println(tempString);
    client.publish("esp32/temperature", tempString);

    humidity = bme.readHumidity();

    // Convert the value to a char array
    char humString[8];
    dtostrf(humidity, 1, 2, humString);
    Serial.print("Humidity: ");
    Serial.println(humString);
    client.publish("esp32/humidity", humString);
    }*/
}
